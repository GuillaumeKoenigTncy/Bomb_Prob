//Written by Nick Koumaris
//info@educ8s.tv
// https://www.electronics-lab.com/project/rfid-rc522-arduino-uno-oled-display/

#include <MFRC522.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <SPI.h>

#define OLED_RESET 4
Adafruit_SSD1306 display(OLED_RESET);

#define SS_PIN 10
#define RST_PIN 9
 
MFRC522 rfid(SS_PIN, RST_PIN); // Instance of the class
MFRC522::MIFARE_Key key; 

int code[] = {249, 214, 197, 212}; //This is the stored UID
int codeRead = 0;
String uidString;

int d1 = 0;
int d2 = 0;
int d3 = 0;
int d4 = 0;

int selected = 1;

int pushButtonGD = 2;
int lastButtonStateGD  = 0;
int pushButtonHB = 3;
int lastButtonStateHB  = 0;
int pushButtonCode = 4;
int lastButtonStateCode = 0;

void setup() {

  Serial.begin(9600);
  SPI.begin(); // Init SPI bus
  rfid.PCD_Init(); // Init MFRC522 
  
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);  // initialize with the I2C addr 0x3D (for the 128x64)

  display.clearDisplay();
  display.display();
  display.setTextColor(WHITE); // or BLACK);
  display.setTextSize(2);
  display.setCursor(10,0); 
  display.print(" 0 0 0 0 ");
  display.display();

  pinMode(8, OUTPUT);
  pinMode(7, OUTPUT);
  pinMode(6, OUTPUT);

  pinMode(pushButtonGD, INPUT);
  pinMode(pushButtonHB, INPUT);
  pinMode(pushButtonCode, INPUT);
  
}

void loop() {

  // Bouton haut et bas //
  int buttonStateHB = digitalRead(pushButtonHB);
  if (buttonStateHB != lastButtonStateHB) {
    if (buttonStateHB == 1) {
      if (selected == 1){
        d1 = d1 + 1;
        if (d1 == 10) { d1 = 0; }
      }
      if (selected == 2){
        d2 = d2 + 1;
        if (d2 == 10) { d2 = 0; }
      }
      if (selected == 3){
        d3 = d3 + 1;
        if (d3 == 10) { d3 = 0; }
      }   
      if (selected == 4){
        d4 = d4 + 1;
        if (d4 == 10) { d4 = 0; }
      }  
    }
    lastButtonStateHB = buttonStateHB;
  }

  // Bouton gauche et droite //
  int buttonStateGD = digitalRead(pushButtonGD);
  if (buttonStateGD != lastButtonStateGD) {
    if (buttonStateGD == 1) {
      selected = selected + 1;
      if (selected == 5){
        selected = 1;
      }
    }
    lastButtonStateGD = buttonStateGD;
  }

  // Bouton validation code //
  int buttonStateCode = digitalRead(pushButtonCode);
  if (buttonStateCode != lastButtonStateCode) {
    if (buttonStateCode == 1) {
      if (d1 == 1 & d2 == 3){
        Serial.println("yes");
        digitalWrite(6, HIGH);
      }else{
       Serial.println("no");
       digitalWrite(8, HIGH);
       }
    }
    lastButtonStateCode = buttonStateCode;
  }

  // Display digit //
  display.clearDisplay();
  display.setTextColor(WHITE); // or BLACK);
  display.setTextSize(2);
  display.setCursor(10,0);
  display.print(" ");
  display.print(d1);
  display.print(" ");
  display.print(d2);
  display.print(" ");
  display.print(d3);
  display.print(" ");
  display.print(d4);
  display.display();

  if(rfid.PICC_IsNewCardPresent()){
      readRFID();
  }

}

void readRFID(){
  
  rfid.PICC_ReadCardSerial();
  Serial.print(F("\nPICC type: "));
  MFRC522::PICC_Type piccType = rfid.PICC_GetType(rfid.uid.sak);
  Serial.println(rfid.PICC_GetTypeName(piccType));

  // Check is the PICC of Classic MIFARE type
  if (piccType != MFRC522::PICC_TYPE_MIFARE_MINI &&  
    piccType != MFRC522::PICC_TYPE_MIFARE_1K &&
    piccType != MFRC522::PICC_TYPE_MIFARE_4K) {
    Serial.println(F("Your tag is not of type MIFARE Classic."));
    return;
  }

  clearUID();
 
  Serial.println("Scanned PICC's UID:");
  printDec(rfid.uid.uidByte, rfid.uid.size);

  uidString = String(rfid.uid.uidByte[0])+" "+String(rfid.uid.uidByte[1])+" "+String(rfid.uid.uidByte[2])+ " "+String(rfid.uid.uidByte[3]);
  
  printUID();

  int i = 0;
  boolean match = true;
  while(i<rfid.uid.size){
    if(!(rfid.uid.uidByte[i] == code[i])){
         match = false;
    }
    i++;
  }

  if(match){
    Serial.println("\nI know this card!");
    digitalWrite(7, HIGH);   // turn the LED on (HIGH is the voltage level)
  }else{
    digitalWrite(8, HIGH);
    Serial.println("\nUnknown Card");
  }

    // Halt PICC
  rfid.PICC_HaltA();

  // Stop encryption on PCD
  rfid.PCD_StopCrypto1();
}

void printDec(byte *buffer, byte bufferSize) {
  for (byte i = 0; i < bufferSize; i++) {
    Serial.print(buffer[i] < 0x10 ? " 0" : " ");
    Serial.print(buffer[i], DEC);
  }
}

void clearUID(){
  display.setTextColor(BLACK); // or BLACK);
  display.setTextSize(1);
  display.setCursor(30,20); 
  display.print(uidString);
  display.display();
}

void printUID(){
  display.setTextColor(WHITE); // or BLACK);
  display.setTextSize(1);
  display.setCursor(0,20); 
  display.print("UID: ");
  display.setCursor(30,20); 
  display.print(uidString);
  display.display();
}
