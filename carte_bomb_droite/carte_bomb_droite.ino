/*********
  Rui Santos
  Complete project details at https://randomnerdtutorials.com  
*********/

#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 32 // OLED display height, in pixels

// Declaration for an SSD1306 display connected to I2C (SDA, SCL pins)
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, -1);


int speakerPin = 8; // Définition du buzzer sur la broche 8

int i = 1800;

int pushButtonCode = 6;
int lastButtonStateCode = 0;

void setup() {
  Serial.begin(115200);

  pinMode(7, OUTPUT);
  
  pinMode(pushButtonCode, INPUT);

  pinMode (2, INPUT_PULLUP);
  pinMode (3, INPUT_PULLUP);
  pinMode (4, INPUT_PULLUP);
  pinMode (5, INPUT_PULLUP);

  pinMode (9, INPUT_PULLUP);
  pinMode (10, INPUT_PULLUP);
  pinMode (11, INPUT_PULLUP);
  pinMode (12, INPUT_PULLUP);

  pinMode (13, OUTPUT);

  pinMode (speakerPin, OUTPUT); // Définition de la broche 8 en tant que sortie

  if(!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) { // Address 0x3D for 128x64
    Serial.println(F("SSD1306 allocation failed"));
    for(;;);
  }
  delay(2000);
  display.clearDisplay();

  display.setTextSize(2);
  display.setTextColor(WHITE);
  display.setCursor(0, 10);
  display.println("Hello, world!");
  display.display();

  pinMode(LED_BUILTIN, OUTPUT);
  
}

void loop() {
  
  analogWrite (speakerPin, 255); // Signal haut sur broche 8
  delay (50); // pendant 50 ms
  analogWrite (speakerPin, 0); // Signal bas sur broche 8
  delay (10); // pendant 10 ms

  // Bouton validation code //
  int buttonStateCode = digitalRead(pushButtonCode);
  if (buttonStateCode != lastButtonStateCode) {
    if (buttonStateCode == 1) {

      int v2 = digitalRead(2);
      int v3 = digitalRead(3);
      int v4 = digitalRead(4);
      int v5 = digitalRead(5);

      int v9 = digitalRead(9);
      int v10 = digitalRead(10);
      int v11 = digitalRead(11);
      int v12 = digitalRead(12);

      if(v2 == 1 & v3 == 0 & v4 == 1 & v5 == 0 & v9 == 0 & v10 == 1 & v11 == 1 & v12 == 0){
        digitalWrite(7, HIGH);  
      }else{
        digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
      }
    }
    lastButtonStateCode = buttonStateCode;
  }

  display.clearDisplay();
  display.setCursor(0, 10);
  display.print(i / 60);
  display.print(":");
  display.print(i % 60);
  display.display(); 
  delay(1000);

  i = i - 1;

}
